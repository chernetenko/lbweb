package ru.stqa.pft.webtests.appmanager;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.util.concurrent.TimeUnit;

public class PasswordChangeHelper extends HelperBase {

    public PasswordChangeHelper(WebDriver driver) {
        super(driver);
    }

    public void changePassword() throws InterruptedException {
        TimeUnit.SECONDS.sleep(5);
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div[2]/div[8]/div[2]/div/div/div/div[2]/div[3]/div[1]/span")).click();
    }

    public void oldPassword() {
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div[2]/div[8]/div[2]/div/div/div/div[2]/div[3]/div[2]/div[1]/div/input")).sendKeys("password");
    }

    public void NewPassword() {
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div[2]/div[8]/div[2]/div/div/div/div[2]/div[3]/div[2]/div[2]/div/input")).sendKeys("password");
    }

    public void NewPassword2() {
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div[2]/div[8]/div[2]/div/div/div/div[2]/div[3]/div[2]/div[3]/div/input")).sendKeys("password");
    }

    public void SaveOk() throws Exception {
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div[2]/div[8]/div[2]/div/div/div/div[5]")).click();//save
        TimeUnit.SECONDS.sleep(3);
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div[2]/div[8]/div[2]/div/div/div/div[2]/div")).click();//ok
    }
}
