package ru.stqa.pft.webtests.appmanager;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.util.concurrent.TimeUnit;

public class HowToPlayPopUpWelcomeHelper extends HelperBase {

    public HowToPlayPopUpWelcomeHelper(WebDriver driver) {
        super(driver);
    }

    public void ButtonPlayNow() {
        driver.findElement(By.linkText("Play now")).click();
    }

    public void ButtonSkip() throws InterruptedException {
        driver.switchTo().frame(0);
        TimeUnit.SECONDS.sleep(13);
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div[1]/div[2]")).click();
    }

    public void HowToPlay() throws InterruptedException {
        TimeUnit.SECONDS.sleep(15);
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div[2]/div[8]/div[2]/div/div/div/div[3]")).click();
    }

    public void clickPoints() throws InterruptedException {
        TimeUnit.SECONDS.sleep(2);
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div[3]/div[2]/div[1]/div[2]")).click();
    }

    public void FullScreen() throws InterruptedException {
        driver.switchTo().defaultContent();
        TimeUnit.SECONDS.sleep(2);
        driver.findElement(By.xpath("//*[@id=\"fullscreen-btn\"]")).click();//увеличение
        driver.findElement(By.xpath("//*[@id=\"fullscreen-btn\"]")).click();//уменьшение
        }

        public void Close () throws InterruptedException {
            driver.switchTo().frame(0);
            TimeUnit.SECONDS.sleep(2);
            driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div[3]/div[2]/div[2]")).click();
        }
    }
