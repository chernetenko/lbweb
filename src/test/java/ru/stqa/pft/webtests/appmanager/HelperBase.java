package ru.stqa.pft.webtests.appmanager;

import org.openqa.selenium.*;

import java.io.File;

class HelperBase {
    WebDriver driver;


    HelperBase(WebDriver driver) {
        this.driver = driver;
    }

    void type(By locator, String text) {
        click(locator);
        driver.findElement(locator).clear();
        driver.findElement(locator).sendKeys(text);
    }

    void attach(By locator, File file) {
        driver.findElement(locator).sendKeys(file.getAbsolutePath());
    }


    boolean click(By locator) {
        driver.findElement(locator).click();
        return true;
    }


    public boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    public boolean isAlertPresent() {
        try {
            driver.switchTo().alert();
            return true;
        } catch (NoAlertPresentException e) {
            return false;
        }
    }
}