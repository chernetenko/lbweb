package ru.stqa.pft.webtests.appmanager;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ScrollingReviewsHelper extends HelperBase {

    public ScrollingReviewsHelper(WebDriver driver) {
        super(driver);
    }

    public void clickRightButton() {
        driver.findElement(By.xpath("//*[@id=\"middle\"]/section[2]/div/div[2]/div[4]")).click();
        driver.findElement(By.xpath("//*[@id=\"middle\"]/section[2]/div/div[2]/div[4]")).click();
        driver.findElement(By.xpath("//*[@id=\"middle\"]/section[2]/div/div[2]/div[4]")).click();
    }

    public void clickLeftButton() {
        driver.findElement(By.xpath("//*[@id=\"middle\"]/section[2]/div/div[2]/div[3]")).click();
        driver.findElement(By.xpath("//*[@id=\"middle\"]/section[2]/div/div[2]/div[3]")).click();
        driver.findElement(By.xpath("//*[@id=\"middle\"]/section[2]/div/div[2]/div[3]")).click();
    }

    public void clickPointButton() {
        driver.findElement(By.xpath("//*[@id=\"middle\"]/section[2]/div/div[2]/div[2]/span[2]")).click();
        driver.findElement(By.xpath("//*[@id=\"middle\"]/section[2]/div/div[2]/div[2]/span[3]")).click();
        driver.findElement(By.xpath("//*[@id=\"middle\"]/section[2]/div/div[2]/div[2]/span[4]")).click();
    }

}
