package ru.stqa.pft.webtests.appmanager;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import ru.stqa.pft.webtests.model.AccountData;

import java.util.concurrent.TimeUnit;

public class AskTheDevelopersHelper extends HelperBase {

    public AskTheDevelopersHelper(WebDriver driver) {
        super(driver);
    }

    public void Settings (){
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div[2]/div[4]/div[2]/div[1]/div[2]")).click();
    }

    public void AskTheDevelopers () {
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div[2]/div[8]/div[2]/div/div/div/div[7]")).click();
    }

    public void FillYourName (String username) {
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div[2]/div[8]/div[2]/div/div/div/div[2]/div[1]/div[1]/div/div/input")).click();
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div[2]/div[8]/div[2]/div/div/div/div[2]/div[1]/div[1]/div/div/input")).clear();
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div[2]/div[8]/div[2]/div/div/div/div[2]/div[1]/div[1]/div/div/input")).sendKeys(username);
    }

    public void FillEmail (String email) {
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div[2]/div[8]/div[2]/div/div/div/div[2]/div[1]/div[2]/div/div/input")).click();
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div[2]/div[8]/div[2]/div/div/div/div[2]/div[1]/div[2]/div/div/input")).clear();
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div[2]/div[8]/div[2]/div/div/div/div[2]/div[1]/div[2]/div/div/input")).sendKeys(email);
    }


    public void FillSubject (String subject) {
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div[2]/div[8]/div[2]/div/div/div/div[2]/div[2]/div[1]/div/div/input")).click();
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div[2]/div[8]/div[2]/div/div/div/div[2]/div[2]/div[1]/div/div/input")).clear();
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div[2]/div[8]/div[2]/div/div/div/div[2]/div[2]/div[1]/div/div/input")).sendKeys(subject);
    }

    public void FillYourMessage (String message) {
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div[2]/div[8]/div[2]/div/div/div/div[2]/div[2]/div[2]/div/div/textarea")).click();
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div[2]/div[8]/div[2]/div/div/div/div[2]/div[2]/div[2]/div/div/textarea")).clear();
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div[2]/div[8]/div[2]/div/div/div/div[2]/div[2]/div[2]/div/div/textarea")).sendKeys(message);
    }

    public void FillAttach (AccountData accountData) throws InterruptedException {
        TimeUnit.SECONDS.sleep(1);
        attach((By.xpath("//*[@id=\"root\"]/div/div/div/div[2]/div[8]/div[2]/div/div/div/div[2]/div[1]/div[4]/div/input")), accountData.getPhoto());
    }

    public void Send () throws InterruptedException {
        TimeUnit.SECONDS.sleep(2);
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div[2]/div[8]/div[2]/div/div/div/div[3]")).click();
    }

    public void OK () throws InterruptedException {
        TimeUnit.SECONDS.sleep(4);
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div[2]/div[8]/div[2]/div/div/div/div[2]/div")).click();
    }
}
