package ru.stqa.pft.webtests.appmanager;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.util.concurrent.TimeUnit;


public class WithdrawHelper extends HelperBase {

    public WithdrawHelper(WebDriver driver) {
        super(driver);
    }

    public void buttonWithdraw() throws InterruptedException {
        TimeUnit.SECONDS.sleep(9);
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div[2]/div[2]/div[3]/div[2]")).click();
    }

    public void fillWithdraw() throws InterruptedException {
        TimeUnit.SECONDS.sleep(5);
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div[2]/div[8]/div[2]/div/div/div/div[5]/div[2]/input")).clear();
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div[2]/div[8]/div[2]/div/div/div/div[5]/div[2]/input")).sendKeys("10");
    }

    public void clickWithdraw() {
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div[2]/div[8]/div[2]/div/div/div/div[6]/span")).click();
    }

    public void clickOK() throws InterruptedException {
        TimeUnit.SECONDS.sleep(5);
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div[2]/div[8]/div[2]/div/div/div/div[2]/div")).click();
    }
}