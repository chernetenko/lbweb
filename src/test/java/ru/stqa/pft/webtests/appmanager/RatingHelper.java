package ru.stqa.pft.webtests.appmanager;

import org.openqa.selenium.*;
import org.openqa.selenium.By;

public class RatingHelper extends HelperBase {

    public RatingHelper(WebDriver driver) {
        super(driver);
    }

    public void clickButtonSettings() {
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div[2]/div[4]/div[2]/div[1]/div[2]")).click();
    }

    public void clickButtonRating() {
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div[2]/div[8]/div[2]/div/div/div/div[4]")).click();
    }

    public void clickButtonMonthlyTop() {
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div[2]/div[8]/div[2]/div/div/div/div[1]/div[2]/div")).click();
    }

    public void clickButtonDailyTop() {
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div[2]/div[8]/div[2]/div/div/div/div[1]/div[2]/div")).click();
    }

    public void clickButtonBackward() {
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div[2]/div[8]/div[2]/div/div/div/div[4]")).click();
    }
}

