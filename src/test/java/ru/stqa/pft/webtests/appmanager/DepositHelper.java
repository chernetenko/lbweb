package ru.stqa.pft.webtests.appmanager;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.util.concurrent.TimeUnit;

public class DepositHelper extends HelperBase {

    DepositHelper(WebDriver driver) {
        super(driver);
    }

    public void ButtonDeposit50() throws InterruptedException {
        TimeUnit.SECONDS.sleep(4);
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div[2]/div[2]/div[2]/div[2]")).click();//кнопка депозит
        TimeUnit.SECONDS.sleep(2);
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div[2]/div[8]/div[2]/div/div/div/div[3]/div[3]/div/span")).click();//кнопка 100€
        TimeUnit.SECONDS.sleep(2);
        driver.findElement(By.xpath("//*[@id=\"footer\"]/div/div/p[1]/a")).click();//кнопка Back to shop
    }

    public void EnterTheAmount() throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(driver, 20);
        driver.switchTo().frame(0);
        WebElement element1 = wait.until(
                ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"root\"]/div/div/div/div[2]/div[2]/div[2]/div[2]")));
        TimeUnit.SECONDS.sleep(5);
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div[2]/div[2]/div[2]/div[2]")).click();//кнопка депозит
        TimeUnit.SECONDS.sleep(2);
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div[2]/div[8]/div[2]/div/div/div/div[4]/div[2]/input")).click();
        TimeUnit.SECONDS.sleep(1);
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div[2]/div[8]/div[2]/div/div/div/div[4]/div[2]/input")).clear();
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div[2]/div[8]/div[2]/div/div/div/div[4]/div[2]/input")).sendKeys("50");
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div[2]/div[8]/div[2]/div/div/div/div[4]/div[3]/span")).click();
        TimeUnit.SECONDS.sleep(3);
        driver.findElement(By.xpath("//*[@id=\"footer\"]/div/div/p[1]/a")).click();//кнопка Back to shop
        TimeUnit.SECONDS.sleep(2);
        driver.switchTo().frame(0);
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div[2]/div[1]/div[1]/div[1]")).click();//
        TimeUnit.SECONDS.sleep(3);
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div[2]/div[8]/div[2]/div/div/div/div[2]/div[1]")).click();//
    }
}