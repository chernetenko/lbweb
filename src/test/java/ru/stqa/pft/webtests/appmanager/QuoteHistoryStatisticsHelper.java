package ru.stqa.pft.webtests.appmanager;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class QuoteHistoryStatisticsHelper extends HelperBase {

    QuoteHistoryStatisticsHelper (WebDriver driver) {
        super(driver);
    }

    public void Settings () {
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div[2]/div[4]/div[2]/div[1]/div[2]")).click();
    }

    public void QuoteHistoryStatistics () {
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div[2]/div[8]/div[2]/div/div/div/div[5]")).click();
    }

    public void Statistics () {
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div[2]/div[8]/div[2]/div/div/div/div[1]/div[2]/div")).click();
    }

    public void QuoteHistory () {
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div[2]/div[8]/div[2]/div/div/div/div[1]/div[1]/div")).click();
    }

    public void Currency () { //Перевлючение валюты
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div[2]/div[8]/div[2]/div/div/div/div[2]/div/div[2]/div[1]/div/div[1]/div[2]")).click();//GBPUSD
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div[2]/div[8]/div[2]/div/div/div/div[2]/div/div[1]/span[1]")).click();//1 min
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div[2]/div[8]/div[2]/div/div/div/div[2]/div/div[1]/span[2]")).click();//5 min
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div[2]/div[8]/div[2]/div/div/div/div[2]/div/div[1]/span[2]")).click();//15 min
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div[2]/div[8]/div[2]/div/div/div/div[2]/div/div[1]/span[4]")).click();//30 min
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div[2]/div[8]/div[2]/div/div/div/div[2]/div/div[1]/span[5]")).click();//1 hour
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div[2]/div[8]/div[2]/div/div/div/div[2]/div/div[2]/div[1]/div/div[1]/div[3]")).click();//USDCHF
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div[2]/div[8]/div[2]/div/div/div/div[2]/div/div[2]/div[1]/div/div[1]/div[4]")).click();//USDJPY
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div[2]/div[8]/div[2]/div/div/div/div[2]/div/div[2]/div[1]/div/div[1]/div[1]")).click();//EUR/USD
    }

}
