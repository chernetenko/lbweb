package ru.stqa.pft.webtests.appmanager;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class FourLinksInTheFooterHelper extends HelperBase {

    public FourLinksInTheFooterHelper(WebDriver driver) {
        super(driver);
    }

    public void TermsAndconditions () {
        driver.findElement(By.linkText("Terms and Conditions")).click();
    }

    public void AntiMoneyLa () {
        driver.findElement(By.linkText("Anti Money Laundering")).click();
    }

    public void PrivacyPolicy () {
        driver.findElement(By.linkText("Privacy policy")).click();
    }

    public void NoticeOfRisks  () {
        driver.findElement(By.linkText("Notice of risks")).click();
    }

}
