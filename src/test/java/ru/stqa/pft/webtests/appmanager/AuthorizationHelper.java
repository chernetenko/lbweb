package ru.stqa.pft.webtests.appmanager;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class AuthorizationHelper extends HelperBase {

    public AuthorizationHelper(WebDriver driver) {
        super(driver);
    }

    public void UserProfile() {
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div[2]/div[4]/div[1]/div/div[1]/div[2]/div")).click();
    }

    public void Email(String email) {
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div[2]/div[8]/div[2]/div/div/div/div[3]/div[1]/div/input")).click();
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div[2]/div[8]/div[2]/div/div/div/div[3]/div[1]/div/input")).clear();
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div[2]/div[8]/div[2]/div/div/div/div[3]/div[1]/div/input")).sendKeys(email);
    }

    public void Password(String password) {
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div[2]/div[8]/div[2]/div/div/div/div[3]/div[2]/div/input")).click();
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div[2]/div[8]/div[2]/div/div/div/div[3]/div[2]/div/input")).clear();
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div[2]/div[8]/div[2]/div/div/div/div[3]/div[2]/div/input")).sendKeys(password);
    }

    public void ButtonLogin() throws InterruptedException {
        TimeUnit.SECONDS.sleep(5);
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div[2]/div[8]/div[2]/div/div/div/div[6]")).click();
    }

    public void ButtonLogOut() throws InterruptedException {
        TimeUnit.SECONDS.sleep(5);
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div[2]/div[1]/div[1]/div[1]")).click();//logout
        TimeUnit.SECONDS.sleep(1);
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div[2]/div[8]/div[2]/div/div/div/div[2]/div[1]")).click();//ok
    }
}

