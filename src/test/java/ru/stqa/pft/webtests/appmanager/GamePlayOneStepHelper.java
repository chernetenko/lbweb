package ru.stqa.pft.webtests.appmanager;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import java.util.concurrent.TimeUnit;

public class GamePlayOneStepHelper extends HelperBase {

    public GamePlayOneStepHelper(WebDriver driver) {
        super(driver);
    }

    public void PredictionUp() throws Exception {
        TimeUnit.SECONDS.sleep(10);
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div[2]/ul/div[1]/li/div[1]")).click();//прогноз
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div[2]/div[6]/div[2]")).click();//кнопка GO
    }

    public boolean win(By xpath) throws Exception {
        TimeUnit.SECONDS.sleep(10);
        return isElementPresent(By.xpath("//*[@id=\"root\"]/div/div/div/div[2]/div[8]/div[2]/div/div/div/div[1]"));
    }

    public void overlay() throws Exception {
        TimeUnit.SECONDS.sleep(5);
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div[2]/div[4]/div[2]/div[1]/div[2]")).click();//шестерёнка
    }
}