package ru.stqa.pft.webtests.appmanager;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class AuthorizationNetworksHelper extends HelperBase {

    AuthorizationNetworksHelper(WebDriver driver) {
        super(driver);
    }

    public void AuthorizationFB() throws Exception {
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div[2]/div[8]/div[2]/div/div/div/div[4]/div[2]/ul/li[1]/a")).click();//кнопка FB
        driver.findElement(By.xpath("//*[@id=\"email\"]")).click();//поле email
        driver.findElement(By.xpath("//*[@id=\"email\"]")).clear();
        driver.findElement(By.xpath("//*[@id=\"email\"]")).sendKeys("spbqa2016@gmail.com");
        driver.findElement(By.xpath("//*[@id=\"pass\"]")).click();//поле пароль
        driver.findElement(By.xpath("//*[@id=\"pass\"]")).clear();
        driver.findElement(By.xpath("//*[@id=\"pass\"]")).sendKeys("Ruspb2017!");
        driver.findElement(By.xpath("//*[@id=\"loginbutton\"]")).click();//кнопка Вход
    }

    public void AuthorizationG() throws Exception {
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div[2]/div[8]/div[2]/div/div/div/div[4]/div[2]/ul/li[2]/a")).click();//кнопка G
        TimeUnit.SECONDS.sleep(1);
        driver.findElement(By.xpath("//*[@id=\"identifierId\"]")).click();//поле email
        driver.findElement(By.xpath("//*[@id=\"identifierId\"]")).clear();
        driver.findElement(By.xpath("//*[@id=\"identifierId\"]")).sendKeys("autotestlb@gmail.com");
        TimeUnit.SECONDS.sleep(1);
        driver.findElement(By.xpath("//*[@id=\"identifierNext\"]/content/span")).click();//кнопка Далее
        TimeUnit.SECONDS.sleep(2);
        driver.findElement(By.xpath("//*[@id=\"password\"]/div[1]/div/div[1]/input")).click();//поле пароль
        driver.findElement(By.xpath("//*[@id=\"password\"]/div[1]/div/div[1]/input")).clear();
        driver.findElement(By.xpath("//*[@id=\"password\"]/div[1]/div/div[1]/input")).sendKeys("Ruspb2017!");
        driver.findElement(By.xpath("//*[@id=\"passwordNext\"]/content/span")).click();//далее
        TimeUnit.SECONDS.sleep(4);
        driver.findElement(By.xpath("//*[@id=\"header\"]/div/div/div[2]/div/label/span/img")).click();//ru
        driver.findElement(By.xpath("//*[@id=\"header\"]/div/div/div[2]/div/ul/li[2]/a/img")).click();//en
        driver.switchTo().frame(0);
        TimeUnit.SECONDS.sleep(4);
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div[2]/div[1]/div[1]/div[1]")).click();//
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div[2]/div[8]/div[2]/div/div/div/div[2]/div[1]")).click();//да
    }
}
