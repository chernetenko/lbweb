package ru.stqa.pft.webtests.appmanager;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import ru.stqa.pft.webtests.model.AccountData;
import java.util.concurrent.TimeUnit;

public class EditProfileHelper extends HelperBase {

    EditProfileHelper(WebDriver driver) {
        super(driver);
    }

    public void UserProfile() throws InterruptedException {
        TimeUnit.SECONDS.sleep(5);
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div[2]/div[4]/div[1]/div/div[1]/div[2]")).click();
    }

    public void EditProfile() throws InterruptedException {
        TimeUnit.SECONDS.sleep(2);
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div[2]/div[8]/div[2]/div/div/div/div[2]/div[1]/span")).click();
    }

    public void EditFields(String name, String lastName) {
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div[2]/div[8]/div[2]/div/div/div/div[2]/div[2]/div[2]/div[1]/div[1]/div/input")).clear();
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div[2]/div[8]/div[2]/div/div/div/div[2]/div[2]/div[2]/div[1]/div[1]/div/input")).sendKeys(name);
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div[2]/div[8]/div[2]/div/div/div/div[2]/div[2]/div[2]/div[1]/div[2]/div/input")).clear();
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div[2]/div[8]/div[2]/div/div/div/div[2]/div[2]/div[2]/div[1]/div[2]/div/input")).sendKeys(lastName);
    }

    public void ChangeAvatar(AccountData accountData) throws InterruptedException {
        TimeUnit.SECONDS.sleep(10);
        attach((By.xpath("//*[@id=\"root\"]/div/div/div/div[2]/div[8]/div[2]/div/div/div/div[2]/div[2]/div[1]/input")), accountData.getPhoto());
        TimeUnit.SECONDS.sleep(3);
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div[2]/div[8]/div[2]/div/div/div/div[5]")).click();
    }
}