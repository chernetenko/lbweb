package ru.stqa.pft.webtests.appmanager;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.BrowserType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.Properties;
import java.util.concurrent.TimeUnit;


public class ApplicationManager {
    private final Properties properties;
    public WebDriver driver;

    private ScrollingReviewsHelper scrollingReviewsHelper;
    private AuthorizationHelper authorizationHelper;
    private QuoteHistoryStatisticsHelper quoteHistoryStatisticsHelper;
    private RatingHelper ratingHelper;
    private DepositHelper depositHelper;
    private FourLinksInTheFooterHelper fourLinksInTheFooterHelper;
    private AskTheDevelopersHelper askTheDevelopersHelper;
    private HowToPlayPopUpWelcomeHelper howToPlayPopUpWelcomeHelper;
    private EditProfileHelper editProfileHelper;
    private PasswordChangeHelper passwordChangeHelper;
    private GamePlayOneStepHelper gamePlayOneStepHelper;
    private AuthorizationNetworksHelper authorizationNetworksHelper;
    private WithdrawHelper withdrawHelper;


    private String browser;

    public ApplicationManager(String browser) {

        this.browser = browser;
        properties = new Properties();
    }

    public void init() throws IOException, InterruptedException {
        String target = System.getProperty("target", "local");
        properties.load(new FileReader(new File(String.format("src/test/resources/%s.properties", target))));


        if ("".equals(properties.getProperty("selenium.server"))) {
            if (browser.equals(BrowserType.FIREFOX)) {
                driver = new FirefoxDriver();
            } else if (browser.equals(BrowserType.CHROME)) {
                driver = new ChromeDriver();
            }
        } else {
            DesiredCapabilities capabilities = new DesiredCapabilities();
            capabilities.setBrowserName(browser);
            driver = new RemoteWebDriver(new URL(properties.getProperty("selenium.server")), capabilities);
        }

        scrollingReviewsHelper = new ScrollingReviewsHelper(driver);
        authorizationHelper = new AuthorizationHelper(driver);
        quoteHistoryStatisticsHelper = new QuoteHistoryStatisticsHelper(driver);
        ratingHelper = new RatingHelper(driver);
        depositHelper = new DepositHelper(driver);
        fourLinksInTheFooterHelper = new FourLinksInTheFooterHelper(driver);
        askTheDevelopersHelper = new AskTheDevelopersHelper(driver);
        howToPlayPopUpWelcomeHelper = new HowToPlayPopUpWelcomeHelper(driver);
        editProfileHelper = new EditProfileHelper(driver);
        passwordChangeHelper = new PasswordChangeHelper(driver);
        gamePlayOneStepHelper = new GamePlayOneStepHelper(driver);
        authorizationNetworksHelper = new AuthorizationNetworksHelper(driver);
        withdrawHelper = new WithdrawHelper(driver);

        driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
        driver.get(properties.getProperty("web.baseUrl"));
    }

    public void stop() {
        driver.quit();
    } // закрытие браузера


    public ScrollingReviewsHelper getScrollingReviewsHelper() {
        return scrollingReviewsHelper;
    }

    public AuthorizationHelper getAuthorizationHelper() {
        return authorizationHelper;
    }

    public QuoteHistoryStatisticsHelper getQuoteHistoryStatisticsHelper() {
        return quoteHistoryStatisticsHelper;
    }

    public RatingHelper getRatingHelper() {
        return ratingHelper;
    }

    public DepositHelper getDepositHelper() {
        return depositHelper;
    }

    public FourLinksInTheFooterHelper getFourLinksInTheFooterHelper() {
        return fourLinksInTheFooterHelper;
    }

    public AskTheDevelopersHelper getAskTheDevelopersHelper() {
        return askTheDevelopersHelper;
    }

    public HowToPlayPopUpWelcomeHelper getHowToPlayPopUpWelcomeHelper() {
        return howToPlayPopUpWelcomeHelper;
    }

    public EditProfileHelper getEditProfileHelper() {
        return editProfileHelper;
    }

    public PasswordChangeHelper getPasswordChangeHelper() {
        return passwordChangeHelper;
    }

    public GamePlayOneStepHelper getGamePlayOneStepHelper() {
        return gamePlayOneStepHelper;
    }

    public AuthorizationNetworksHelper getAuthorizationNetworksHelper() {
        return authorizationNetworksHelper;
    }

    public WithdrawHelper getWithdrawHelper() {
        return withdrawHelper;
    }

}