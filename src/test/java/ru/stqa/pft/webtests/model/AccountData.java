package ru.stqa.pft.webtests.model;

import java.io.File;

public class AccountData {

    private File photo;


    public File getPhoto() {
        return photo;
    }

    public AccountData withPhoto(File photo) {
        this.photo = photo;
        return this;
    }

}
