package ru.stqa.pft.webtests.model;

import java.io.File;

public class SupportData {
  private String yourname;
  private String email;
  private String subject;
  private String message;
  private File photo;

  public SupportData(String yourname, String email, String subject, String message) {
    this.yourname = yourname;
    this.email = email;
    this.subject = subject;
    this.message = message;
  }

  public File getPhoto() {
    return photo;
  }

  public String getYourname() {

    return yourname;
  }

  public String getEmail() {

    return email;
  }

  public String getSubject() {

    return subject;
  }

  public String getMessage() {

    return message;
  }

  public SupportData withPhoto(File photo) {
    this.photo = photo;
    return this;
  }
}