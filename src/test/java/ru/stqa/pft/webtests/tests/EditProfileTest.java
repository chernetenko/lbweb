package ru.stqa.pft.webtests.tests;

import org.testng.annotations.Test;
import ru.stqa.pft.webtests.model.AccountData;

import java.io.File;

public class EditProfileTest extends TestBase {

    @Test

    public void testEditProfile() throws Exception {
        Skip();
        Authorization();
        app.getEditProfileHelper().UserProfile();
        app.getEditProfileHelper().EditProfile();
        app.getEditProfileHelper().EditFields("Bob", "Last");
        File photo = new File("src/test/resources/photo.jpg");
        app.getEditProfileHelper().ChangeAvatar(new AccountData().withPhoto(photo));
    }
}