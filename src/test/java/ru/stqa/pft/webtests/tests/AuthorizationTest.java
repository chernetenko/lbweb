package ru.stqa.pft.webtests.tests;

import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

public class AuthorizationTest extends TestBase {

    @Test
    public void Authorization() throws Exception {
        WebDriverWait wait = new WebDriverWait(app.driver, 20);
        Skip();
        app.getAuthorizationHelper().UserProfile();
        app.getAuthorizationHelper().Email("autotestlb@gmail.com");
        app.getAuthorizationHelper().Password("123456");
        app.getAuthorizationHelper().ButtonLogin();
        app.getAuthorizationHelper().ButtonLogOut();
    }
}