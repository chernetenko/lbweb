package ru.stqa.pft.webtests.tests;

import org.testng.annotations.Test;


public class PasswordChangeTest extends TestBase {

    @Test

    public void testPasswordChange() throws Exception {
        Skip();
        Authorization();
        app.getEditProfileHelper().UserProfile();
        app.getPasswordChangeHelper().changePassword();
        app.getPasswordChangeHelper().oldPassword();
        app.getPasswordChangeHelper().NewPassword();
        app.getPasswordChangeHelper().NewPassword2();
        app.getPasswordChangeHelper().SaveOk();
    }
}
