package ru.stqa.pft.webtests.tests;

import org.testng.annotations.Test;

public class FourLinksInTheFooterTest extends TestBase {

    @Test

    public void testRating() {
        app.getFourLinksInTheFooterHelper().TermsAndconditions();
        app.getFourLinksInTheFooterHelper().AntiMoneyLa();
        app.getFourLinksInTheFooterHelper().PrivacyPolicy();
        app.getFourLinksInTheFooterHelper().NoticeOfRisks();
    }
}
