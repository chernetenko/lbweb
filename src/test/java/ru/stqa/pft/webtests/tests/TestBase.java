package ru.stqa.pft.webtests.tests;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.BrowserType;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.*;
import ru.stqa.pft.webtests.appmanager.ApplicationManager;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

public class TestBase {

    Logger logger = LoggerFactory.getLogger(TestBase.class);

    static ApplicationManager app
            = new ApplicationManager(System.getProperty("browser", BrowserType.CHROME));
    //public static ApplicationManager app = new ApplicationManager(BrowserType.FIREFOX);

    @BeforeMethod(alwaysRun = true)
    public void setUp() throws Exception {
        app.init();
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown() throws Exception {
        app.stop();
    }

    @BeforeMethod
    public void logTestStart(Method m, Object[] p) {
        logger.info("Start test " + m.getName() + " with parameters " + Arrays.asList(p));
    }

    @AfterMethod(alwaysRun = true)
    public void logTestStop(Method m) {
        logger.info("Stop test " + m.getName());
    }

    void clickInFreeScreenArea() { //клик по пустому экрану для закрытия поп-апа
        WebElement element = app.driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div[2]/div[6]/div[2]"));
        JavascriptExecutor executor = (JavascriptExecutor) app.driver;
        executor.executeScript("arguments[0].click();", element);
    }

    public void Authorization() throws Exception {
        WebDriverWait wait = new WebDriverWait(app.driver, 20);
        app.getAuthorizationHelper().UserProfile();
        app.getAuthorizationHelper().Email("autotestlb@gmail.com");
        app.getAuthorizationHelper().Password("password");
        app.getAuthorizationHelper().ButtonLogin();
    }

    public void Skip () throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(app.driver, 20);
        app.driver.findElement(By.linkText("Play now")).click();
        app.driver.switchTo().frame(0);
        WebElement element1 = wait.until(
                ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"root\"]/div/div/div/div[1]/div[2]")));
        TimeUnit.SECONDS.sleep(1);
        app.driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div[1]/div[2]")).click();
        WebElement element2 = wait.until(
                ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"root\"]/div/div/div/div[2]/div[8]/div[2]/div/div/div/div[2]")));
        TimeUnit.SECONDS.sleep(1);
        app.driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div[2]/div[8]/div[2]/div/div/div/div[2]")).click();
    }
}

