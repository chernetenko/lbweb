package ru.stqa.pft.webtests.tests;

import org.testng.annotations.Test;

public class ScrollingReviewsTest extends TestBase {

    @Test

    public void testScrollingReviews() {
        app.getScrollingReviewsHelper().clickRightButton();
        app.getScrollingReviewsHelper().clickLeftButton();
        app.getScrollingReviewsHelper().clickPointButton();
    }
}
