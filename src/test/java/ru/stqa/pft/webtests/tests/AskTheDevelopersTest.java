package ru.stqa.pft.webtests.tests;

import org.testng.annotations.Test;
import ru.stqa.pft.webtests.model.AccountData;
import java.io.File;

public class AskTheDevelopersTest extends TestBase {

    @Test

    public void testAskTheDevelopers() throws InterruptedException {
        Skip();
        app.getAskTheDevelopersHelper().Settings();
        app.getAskTheDevelopersHelper().AskTheDevelopers();
        app.getAskTheDevelopersHelper().FillYourName("Bob");
        app.getAskTheDevelopersHelper().FillEmail("autotestlb@gmail.com");
        app.getAskTheDevelopersHelper().FillSubject("test");
        app.getAskTheDevelopersHelper().FillYourMessage("test1");
        File photo = new File("src/test/resources/photo.jpg");
        app.getAskTheDevelopersHelper().FillAttach(new AccountData().withPhoto(photo));
        app.getAskTheDevelopersHelper().Send();
        app.getAskTheDevelopersHelper().OK();
    }
}