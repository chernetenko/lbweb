package ru.stqa.pft.webtests.tests;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

public class GamePlayOneStepTest extends TestBase {

    @Test

    public void testGamePlayOneStep() throws Exception {
        Skip();
        Authorization();
        app.getGamePlayOneStepHelper().PredictionUp();
        if (!app.getGamePlayOneStepHelper().win(By.xpath("//*[@id=\"root\"]/div/div/div/div[2]/div[8]/div[2]/div/div/div/div[1]"))) {
            app.getGamePlayOneStepHelper().overlay();
        }
    }
}