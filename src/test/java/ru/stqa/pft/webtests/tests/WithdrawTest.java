package ru.stqa.pft.webtests.tests;

import org.testng.annotations.Test;

public class WithdrawTest extends TestBase {

    @Test

    public void testWithdraw() throws Exception {
        Skip();
        Authorization();
        app.getWithdrawHelper().buttonWithdraw();
        app.getWithdrawHelper().fillWithdraw();
        app.getWithdrawHelper().clickWithdraw();
        app.getWithdrawHelper().clickOK();
    }
}
