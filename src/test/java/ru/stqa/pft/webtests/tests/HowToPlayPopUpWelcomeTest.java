package ru.stqa.pft.webtests.tests;

import org.testng.annotations.Test;


public class HowToPlayPopUpWelcomeTest extends TestBase {

    @Test

    public void testHowToPlayPopUpWelcome() throws InterruptedException {
        app.getHowToPlayPopUpWelcomeHelper().ButtonPlayNow();
        app.getHowToPlayPopUpWelcomeHelper().ButtonSkip();
        app.getHowToPlayPopUpWelcomeHelper().HowToPlay();
        app.getHowToPlayPopUpWelcomeHelper().clickPoints();
        app.getHowToPlayPopUpWelcomeHelper().FullScreen();
        app.getHowToPlayPopUpWelcomeHelper().Close();
    }
}

